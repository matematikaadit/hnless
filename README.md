# HNless

https://matematikaadit.gitlab.io/hnless/

[Hacker News](https://news.ycombinator.com/) reader but links only.
Sorted alphabetically. Static. Updated hourly.

# Why?

Experimenting on how likely we will read a story on HN if:

- the score and rank,
- the submission time,
- the submitter,
- or the number of comments,

are unknown to us.

# Result

Not workings. Now it has score.

# License

See [UNLICENSE.txt](UNLICENSE.txt)