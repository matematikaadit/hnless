<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" encoding="UTF-8" indent="yes"/>
  <xsl:template match="/">
    <html>
      <head>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title>{HN}</title>
<!-- stylesheet -->
<style>
body {
  font-size: 20px;
  font-family: sans-serif;

  margin: 0;
  padding: 1em 1em 4em;
  max-width: 50em;
}
ul, ol {
  padding-left: 1em;
}
li {
  margin-top: 0.5em;
}
time, .score {
  font-size: 18px;
  font-family: monospace;
}
.source {
  font-size: 16px;
}
</style>
<!-- end stylesheet -->
      </head>
      <body>
        <h1>{HN}</h1>
        <p>Updated: <time><xsl:value-of select="$time"/></time> UTC. <a href="{$repo}">Repository</a></p>
        <ul>
          <xsl:apply-templates select="//tr[@class='athing']"/>
        </ul>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="tr[@class='athing']">
    <li>
      <!-- score -->
      <xsl:apply-templates select="following-sibling::tr[1]/td/span[@class='score']"/>
      <xsl:text> </xsl:text>
      <!-- link -->
      <xsl:apply-templates select="td/a[@class='storylink']"/>
      <!-- source -->
      <xsl:apply-templates select="td/span[@class='sitebit comhead']"/>
    </li>
  </xsl:template>

  <xsl:template match="span[@class='score']">
    <span class="score">
      <xsl:text>{</xsl:text>
      <xsl:value-of select="substring-before(., ' ')"/>
      <xsl:text>}</xsl:text>
    </span>
  </xsl:template>

  <xsl:template match="span[@class='sitebit comhead']">
    <br/>
    <span class="source">
      <xsl:value-of select="."/>
    </span>
  </xsl:template>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="a/@href[not(starts-with(., 'http:'))][not(starts-with(., 'https:'))]">
    <xsl:attribute name="href">
      <xsl:value-of select="concat($hn, .)"/>
    </xsl:attribute>
  </xsl:template>

  <xsl:variable name="hn">
    <xsl:text>https://news.ycombinator.com/</xsl:text>
  </xsl:variable>

  <xsl:variable name="repo">
    <xsl:text>https://gitlab.com/matematikaadit/hnless</xsl:text>
  </xsl:variable>
</xsl:stylesheet>
