#!/usr/bin/env bash
set -euxo pipefail

target_url='https://news.ycombinator.com'
user_agent='Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36'
accept='text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'
curl_opts=(
    "$target_url"
    -H "Connection: keep-alive"
    -H "Upgrade-Insecure-Requests: 1"
    -H "User-Agent: $user_agent"
    -H "Accept: $accept"
    -H "Accept-Encoding: gzip, deflate, br"
    -H "Accept-Language: en-US,en;q=0.9,id;q=0.8,ja;q=0.7,ru;q=0.6"
    --compressed
)
xsltproc_opts=(
    --html
    --encoding UTF-8
    --stringparam time "$(date --utc +"%H:%M")"
    --output public/index.html
    news.xsl
    -
)

[[ -d public ]] && rm -rf public
mkdir -p public

curl "${curl_opts[@]}" | xsltproc "${xsltproc_opts[@]}"
