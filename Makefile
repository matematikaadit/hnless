build:
	bash build.bash

clean:
	rm -rf public

.PHONY: build clean
